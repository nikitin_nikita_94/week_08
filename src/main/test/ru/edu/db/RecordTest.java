package ru.edu.db;

import org.junit.Test;
import org.mockito.Spy;

import java.math.BigDecimal;

import static org.junit.Assert.*;
import static org.mockito.Mockito.spy;

public class RecordTest {

    @Spy
    Record record = spy(Record.class);

    @Test
    public void allTest() {
        record.setId(5L);
        record.setTitle("Title");
        record.setType("Type");
        record.setPrice(new BigDecimal(200));
        record.setText("Text");
        record.setPublisher("Publisher");
        record.setEmail("Email");
        record.setPhone("Phone");
        record.setPictureUrl("PictureUrl");

        assertEquals(Long.valueOf(5L),record.getId());
        assertEquals("Title",record.getTitle());
        assertEquals("Type",record.getType());
        assertEquals(new BigDecimal(200),record.getPrice());
        assertEquals("Text",record.getText());
        assertEquals("Publisher",record.getPublisher());
        assertEquals("Email",record.getEmail());
        assertEquals("Phone",record.getPhone());
        assertEquals("PictureUrl",record.getPictureUrl());
    }
}