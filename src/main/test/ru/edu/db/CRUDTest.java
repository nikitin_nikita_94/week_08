package ru.edu.db;

import org.junit.Before;
import org.junit.Test;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import java.math.BigDecimal;
import java.sql.*;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CRUDTest {

    private DataSource dataSourceMock = mock(DataSource.class);
    private Connection connection = mock(Connection.class);

    private PreparedStatement statementAll = mock(PreparedStatement.class);
    private PreparedStatement statementId = mock(PreparedStatement.class);
    private PreparedStatement statementInsert = mock(PreparedStatement.class);
    private PreparedStatement statementUpdate = mock(PreparedStatement.class);

    private ResultSet resultSetAll = mock(ResultSet.class);
    private ResultSet resultSetId = mock(ResultSet.class);
    private ResultSet resultSetInsert = mock(ResultSet.class);
    private ResultSet resultSetUpdate = mock(ResultSet.class);

    private CRUD crud;
    private CRUD instance;

    private Record recordUpdate = mock(Record.class);
    private Record recordInsert = mock(Record.class);

    @Before
    public void setupJndi() throws NamingException, SQLException {
        System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                "org.osjava.sj.SimpleContextFactory");
        System.setProperty("org.osjava.sj.jndi.shared", "true");

        Context ic = new InitialContext();
        ic.rebind("java:/comp/env", ic);
        ic.rebind("java:/comp/env/jdbc/dbLink", dataSourceMock);
        ic.rebind("jdbc/dbLink", dataSourceMock);

        Context ctx = new InitialContext();
        Context env = (Context) ctx.lookup("java:/comp/env");
        DataSource dataSource = (DataSource) env.lookup("jdbc/dbLink");
        assertEquals(dataSourceMock, dataSource);

        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(CRUD.SELECT_ALL_SQL)).thenReturn(statementAll);
        when(connection.prepareStatement(CRUD.SELECT_BY_ID)).thenReturn(statementId);
        when(connection.prepareStatement(CRUD.INSERT_SQL, Statement.RETURN_GENERATED_KEYS)).thenReturn(statementInsert);
        when(connection.prepareStatement(CRUD.UPDATE_SQL, Statement.RETURN_GENERATED_KEYS)).thenReturn(statementUpdate);

        when(statementAll.executeQuery()).thenReturn(resultSetAll);
        when(resultSetAll.next()).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSetAll.getString("id")).thenReturn("1").thenReturn("2");
        when(resultSetAll.getString("title")).thenReturn("firstTitle").thenReturn("secondTitle");
        when(resultSetAll.getString("type")).thenReturn("firstType").thenReturn("secondType");
        when(resultSetAll.getString("price")).thenReturn("100").thenReturn("200");
        when(resultSetAll.getString("text")).thenReturn("firstText").thenReturn("secondText");
        when(resultSetAll.getString("publisher")).thenReturn("firstPublisher").thenReturn("secondPublisher");
        when(resultSetAll.getString("email")).thenReturn("firstEmail").thenReturn("secondEmail");
        when(resultSetAll.getString("phone")).thenReturn("firstPhone").thenReturn("secondPhone");
        when(resultSetAll.getString("pictureUrl")).thenReturn("firstPictureUrl").thenReturn("secondPictureUrl");

        when(statementId.executeQuery()).thenReturn(resultSetId);
        when(resultSetId.next()).thenReturn(true).thenReturn(false);
        when(resultSetId.getLong("id")).thenReturn(1L);
        when(resultSetId.getString("title")).thenReturn("title");
        when(resultSetId.getString("type")).thenReturn("type");
        when(resultSetId.getString("price")).thenReturn("2.50");
        when(resultSetId.getString("text")).thenReturn("text");
        when(resultSetId.getString("publisher")).thenReturn("publisher");
        when(resultSetId.getString("email")).thenReturn("email");
        when(resultSetId.getString("phone")).thenReturn("phone");
        when(resultSetId.getString("pictureUrl")).thenReturn("pictureUrl");

        when(statementInsert.executeQuery()).thenReturn(resultSetInsert);
        when(resultSetInsert.next()).thenReturn(true).thenReturn(false);
        when(statementInsert.getGeneratedKeys()).thenReturn(resultSetInsert);
        when(resultSetInsert.getLong(1)).thenReturn(5L);

        when(statementUpdate.executeQuery()).thenReturn(resultSetUpdate);
        when(resultSetUpdate.next()).thenReturn(true).thenReturn(false);
        when(statementUpdate.getGeneratedKeys()).thenReturn(resultSetUpdate);
        when(resultSetUpdate.getLong(1)).thenReturn(6L);

        crud = new CRUD(dataSource);
    }

    @Test
    public void getInstance() {
        instance = CRUD.getInstance();
        assertNotNull(instance);
    }

    @Test
    public void getIndex() {
        List<Record> list = crud.getIndex();
        assertNotNull(list);
        assertEquals(Long.valueOf(0), list.get(0).getId());
        assertEquals("firstTitle", list.get(0).getTitle());
        assertEquals("firstType", list.get(0).getType());
        assertEquals(new BigDecimal("100"), list.get(0).getPrice());
        assertEquals("firstText", list.get(0).getText());
        assertEquals("firstPublisher", list.get(0).getPublisher());
        assertEquals("firstEmail", list.get(0).getEmail());
        assertEquals("firstPhone", list.get(0).getPhone());
        assertEquals(2, list.size());
    }

    @Test
    public void getById() {
        assertEquals("email", crud.getById("1L").getEmail());
    }

    @Test
    public void update() {
        long update = crud.update(recordUpdate);
        assertEquals( 6L,update);
    }

    @Test
    public void insert() {
        long insert = crud.insert(recordInsert);
        assertEquals(5L,insert);
    }
}