package ru.edu.servlets;

import org.junit.Before;
import org.junit.Test;
import ru.edu.db.CRUD;
import ru.edu.db.Record;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static org.mockito.Mockito.*;

public class ViewServletTest {

    public static final String PARAM_MISSING = "/index?error=id_param_missing";
    public static final String ID_NOT_FOUND = "/index?error=id_not_found";
    public static final String VIEW_JSP_PAGE = "/WEB-INF/view.jsp";

    private HttpServletRequest request = mock(HttpServletRequest.class);
    private HttpServletResponse response = mock(HttpServletResponse.class);
    private RequestDispatcher dispatcher = mock(RequestDispatcher.class);

    private ViewServlet viewServlet;
    private CRUD crud;

    private Record record = mock(Record.class);


    @Before
    public void setUp() {
        CRUD.setOverride(mock(CRUD.class));
        crud = CRUD.getInstance();
        viewServlet = new ViewServlet();
    }

    @Test
    public void doGet() throws ServletException, IOException {
        when(request.getParameter("id")).thenReturn("1");
        when(crud.getById("1")).thenReturn(record);
        when(request.getRequestDispatcher(VIEW_JSP_PAGE)).thenReturn(dispatcher);

        viewServlet.doGet(request,response);
        verify(request,times(1)).getParameter("id");
        verify(request,times(1)).setAttribute("record",record);
        verify(dispatcher,times(1)).forward(request,response);
    }

    @Test
    public void paramMissingTest() throws ServletException, IOException {
        when(request.getParameter("id")).thenReturn(null);
        viewServlet.doGet(request,response);
        verify(response,times(1)).sendRedirect(PARAM_MISSING);
    }

    @Test
    public void idNotFoundTest() throws ServletException, IOException {
        when(request.getRequestDispatcher(VIEW_JSP_PAGE)).thenReturn(dispatcher);
        when(request.getParameter("id")).thenReturn("1");
        when(crud.getById("1")).thenReturn(null);

        viewServlet.doGet(request,response);
        verify(request,times(1)).getParameter("id");
        verify(response,times(1)).sendRedirect(ID_NOT_FOUND);
    }
}