package ru.edu.servlets;

import org.junit.Before;
import org.junit.Test;
import ru.edu.db.DataSourceMock;
import ru.edu.db.CRUD;
import ru.edu.db.Record;

import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class IndexServletTest {
    private static final String INDEX_JSP_PAGE = "/WEB-INF/index.jsp";
    public static final String RECORD = "records";

    private HttpServletRequest request = mock(HttpServletRequest.class);
    private HttpServletResponse response = mock(HttpServletResponse.class);
    private RequestDispatcher dispatcher = mock(RequestDispatcher.class);
    private DataSource mockSource;
    private DataSource dataSourceActual;

    private CRUD crud;

    @Before
    public void setUp() throws NamingException {
        dataSourceTest();
        CRUD.setOverride(mock(CRUD.class));
        crud = CRUD.getInstance();
    }

    @Test
    public void doGet() throws ServletException, IOException {
        List<Record> list = new ArrayList<>();
        when(request.getRequestDispatcher(INDEX_JSP_PAGE)).thenReturn(dispatcher);
        when(crud.getIndex()).thenReturn(list);

        IndexServlet indexServlet = new IndexServlet();
        indexServlet.doGet(request, response);
        verify(request, times(1)).setAttribute(RECORD, list);
        verify(dispatcher, times(1)).forward(request, response);
    }

    private void dataSourceTest() throws NamingException {
        DataSourceMock dataSourceMock = new DataSourceMock(mockSource);
        dataSourceActual = dataSourceMock.getDataSource();
        assertEquals(mockSource, dataSourceActual);
    }
}