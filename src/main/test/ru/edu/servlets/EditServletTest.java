package ru.edu.servlets;

import org.junit.Before;
import org.junit.Test;
import ru.edu.db.CRUD;
import ru.edu.db.Record;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class EditServletTest {

    public static final String EDIT_JSP_PAGE = "/WEB-INF/edit.jsp";
    public static final String STATUS_OK = "edit?status=ok&id=";
    public static final String VIEW_REDIRECT = "view?id=";

    private HttpServletResponse response = mock(HttpServletResponse.class);
    private HttpServletRequest request = mock(HttpServletRequest.class);
    private RequestDispatcher dispatcher = mock(RequestDispatcher.class);

    private Record record = mock(Record.class);

    private EditServlet editServlet;
    private CRUD crud;

    @Before
    public void setUp() {
        CRUD.setOverride(mock(CRUD.class));
        crud = CRUD.getInstance();
        editServlet = new EditServlet();
    }

    @Test
    public void doGet() throws ServletException, IOException {
        when(request.getParameter("id")).thenReturn("1");
        when(request.getRequestDispatcher(EDIT_JSP_PAGE)).thenReturn(dispatcher);
        when(crud.getById("1")).thenReturn(record);

        editServlet.doGet(request, response);

        verify(request, times(1)).setAttribute("record", record);
        verify(dispatcher, times(1)).forward(request, response);
    }

    @Test
    public void doPost() throws IOException {
        Map<String, String[]> map = new HashMap();
        map.put("id", new String[]{"1"});
        map.put("title", new String[]{"title"});
        map.put("type", new String[]{"type"});
        map.put("text", new String[]{"text"});
        map.put("price", new String[]{"2500"});
        map.put("publisher", new String[]{"publisher"});
        map.put("email", new String[]{"Email"});
        map.put("phone", new String[]{"phone"});
        map.put("picture_url", new String[]{"picture_url"});
        when(request.getParameterMap()).thenReturn(map);

        editServlet.doPost(request, response);
        verify(response, times(1)).sendRedirect(STATUS_OK + "1");
    }

    @Test
    public void doPostNullTitleTest() throws IOException {
        Map<String, String[]> map = new HashMap();
        map.put("id", new String[]{"1"});
        map.put("title", new String[]{null});
        map.put("type", new String[]{"type"});
        map.put("text", new String[]{"text"});
        map.put("price", new String[]{"2500"});
        map.put("publisher", new String[]{"publisher"});
        map.put("email", new String[]{"Email"});
        map.put("phone", new String[]{"phone"});
        map.put("picture_url", new String[]{"picture_url"});
        when(request.getParameterMap()).thenReturn(map);

        editServlet.doPost(request, response);
        verify(response, times(1)).sendRedirect(VIEW_REDIRECT + "1");
    }

    @Test
    public void doPostEmptyTitleTest() throws IOException {
        Map<String, String[]> map = new HashMap();
        map.put("id", new String[]{"1"});
        map.put("title", new String[]{""});
        map.put("type", new String[]{"type"});
        map.put("text", new String[]{"text"});
        map.put("price", new String[]{"2500"});
        map.put("publisher", new String[]{"publisher"});
        map.put("email", new String[]{"Email"});
        map.put("phone", new String[]{"phone"});
        map.put("picture_url", new String[]{"picture_url"});
        when(request.getParameterMap()).thenReturn(map);

        editServlet.doPost(request, response);
        verify(response, times(1)).sendRedirect(VIEW_REDIRECT + "1");
    }
}