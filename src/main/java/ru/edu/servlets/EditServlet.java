package ru.edu.servlets;

import ru.edu.db.CRUD;
import ru.edu.db.Record;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;

public class EditServlet extends HttpServlet {

    /**
     * Обращаемся к синголтону CRUD.
     */
    private CRUD crud = CRUD.getInstance();

    /**
     * Обрабатываем метод doGet.
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(final HttpServletRequest req,
                        final HttpServletResponse resp)
                        throws ServletException, IOException {
        Record record = null;

        String id = req.getParameter("id");
        if (id != null) {
            record = crud.getById(id);
        }
        req.setAttribute("record", record);
        resp.setCharacterEncoding("UTF-8");
        req.getRequestDispatcher("/WEB-INF/edit.jsp").forward(req, resp);
    }


    /**
     * Обрабатываем метод doPost.
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(final HttpServletRequest req,
                        final HttpServletResponse resp)
                        throws IOException {
        Map form = req.getParameterMap();
        Record record = new Record();

        if (form.containsKey("id")) {
            record.setId(Long.parseLong(getStr(form, "id")));
        }
        record.setTitle(getStr(form, "title"));
        record.setType(getStr(form, "type"));
        record.setText(getStr(form, "text"));
        record.setPrice(new BigDecimal(getStr(form, "price")));
        record.setPublisher(getStr(form, "publisher"));
        record.setEmail(getStr(form, "email"));
        record.setPhone(getStr(form, "phone"));
        record.setPictureUrl(getStr(form, "picture_url"));

        if (record.getTitle() == null || record.getTitle().isEmpty()) {
            resp.sendRedirect("view?id=" + record.getId());
            return;
        }
        crud.save(record);
        resp.sendRedirect("edit?status=ok&id=" + record.getId());
    }

    /**
     * Получаем первую строку.
     * @param form
     * @param id
     * @return String
     */
    private String getStr(final Map form, final String id) {
        return ((String[]) form.get(id))[0];
    }
}
