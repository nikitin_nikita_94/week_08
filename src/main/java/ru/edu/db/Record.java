package ru.edu.db;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class Record {

    /**
     * Id.
     */
    private Long id;

    /**
     * Title.
     */
    private String title;

    /**
     * Type.
     */
    private String type;

    /**
     * Price.
     */
    private BigDecimal price;

    /**
     * Text.
     */
    private String text;

    /**
     * Publisher.
     */
    private String publisher;

    /**
     * Email.
     */
    private String email;

    /**
     * Phone.
     */
    private String phone;

    /**
     * PictureUrl.
     */
    private String pictureUrl;

    /**
     * Short.
     * @param length
     * @return String
     */
    public String getShort(final int length) {
        if (text.length() <= length) {
            return text;
        }
        return text.substring(0, length) + "...";
    }
}
