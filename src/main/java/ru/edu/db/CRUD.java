package ru.edu.db;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;


public class CRUD {

    /**
     * Выбрать все записи в таблице.
     */
    public static final String SELECT_ALL_SQL = "SELECT * FROM records";

    /**
     * Выбрать запись в таблице по ID.
     */
    public static final String SELECT_BY_ID = "SELECT * FROM "
            + "records WHERE id=?";

    /**
     * Вставить новую запись в таблицу.
     */
    public static final String INSERT_SQL = "INSERT INTO records "
            + "(title, type, text, price, publisher, "
            + "email, phone, picture_url)"
            + " VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

    /**
     * Обновить запись в таблице.
     */
    public static final String UPDATE_SQL = "UPDATE records SET"
            + " title=?, type=?, text=?, price=?, publisher=?,"
            + " email=?, phone=?, picture_url=? WHERE id=?";

    /**
     * Instance CRUD.
     */
    private static CRUD instance;

    /**
     * DataSource.
     */
    private final DataSource dataSource;

    /**
     * CRUD override.
     */
    private static CRUD override;

    /**
     * Конструктор.
     * @param source
     */
    public CRUD(final DataSource source) {
        this.dataSource = source;
    }

    /**
     * Синглтон.
     * @return CRUD
     */
    public static CRUD getInstance() {
        synchronized (CRUD.class) {
            if (override != null) {
                return override;
            }
            if (instance == null) {
                try {
                    Context ctx = new InitialContext();
                    Context env = (Context) ctx.
                            lookup("java:/comp/env");
                    DataSource dataSource = (DataSource) env.
                            lookup("jdbc/dbLink");
                    instance = new CRUD(dataSource);
                } catch (Exception e) {
                    throw new RuntimeException(e.getMessage(), e);
                }
            }
        }
        return instance;
    }

    /**
     * Получить все записи.
     *
     * @return List
     */
    public List<Record> getIndex() {
        return query(SELECT_ALL_SQL);
    }

    /**
     * Запрос к базе данных.
     *
     * @param sql
     * @param values
     * @return List
     */
    private List<Record> query(final String sql, final Object... values) {
        try (PreparedStatement statement = getConnection()
                .prepareStatement(sql)) {
            for (int i = 0; i < values.length; i++) {
                statement.setObject(i + 1, values[i]);
            }
            ResultSet resultSet = statement.executeQuery();

            List<Record> result = new ArrayList<>();
            while (resultSet.next()) {
                result.add(map(resultSet));
            }
            return result;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * Создание экземпляра Record с данными
     * из базы данных.
     *
     * @param resultSet
     * @return Record
     * @throws SQLException
     */
    private Record map(final ResultSet resultSet) throws SQLException {
        Record record = new Record();
        record.setId(resultSet.getLong("id"));
        record.setTitle(resultSet.getString("title"));
        record.setType(resultSet.getString("type"));
        record.setPrice(new BigDecimal(resultSet.getString("price")));
        record.setText(resultSet.getString("text"));
        record.setPublisher(resultSet.getString("publisher"));
        record.setEmail(resultSet.getString("email"));
        record.setPhone(resultSet.getString("phone"));
        record.setPictureUrl(resultSet.getString("picture_url"));
        return record;
    }

    /**
     * Соеденение.
     *
     * @return Connection
     */
    private Connection getConnection() {
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * Получение записи по ID.
     *
     * @param id
     * @return Record
     */
    public Record getById(final String id) {
        List<Record> records = query(SELECT_BY_ID, id);
        if (records.isEmpty()) {
            return null;
        }
        return records.get(0);
    }


    /**
     * Запрос к базе данных.
     *
     * @param sql
     * @param values
     * @return Long
     */
    private Long execute(final String sql, final Object... values) {
        try (PreparedStatement statement = getConnection()
                .prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            for (int i = 0; i < values.length; i++) {
                statement.setObject(i + 1, values[i]);
            }

            Long id = null;
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getLong(1);
            }

            return id;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * Сохранение или обновление в базе данных.
     *
     * @param record
     * @return Long
     */
    public Long save(final Record record) {
        if (record.getId() == null) {
            return insert(record);
        } else {
            return update(record);
        }
    }

    /**
     * Обновление записи.
     *
     * @param record
     * @return Long
     */
    public Long update(final Record record) {
       return execute(UPDATE_SQL, record.getTitle(), record.getType(),
                record.getText(), (record.getPrice() == null)
                        ? new BigDecimal(0)
                        : record.getPrice().toPlainString(),
                record.getPublisher(),
                record.getEmail(), record.getPhone(), record.getPictureUrl(),
                record.getId());
    }

    /**
     * Вставка записи.
     *
     * @param record
     * @return Long
     */
    public Long insert(final Record record) {
        return execute(INSERT_SQL, record.getTitle(), record.getType(),
                record.getText(), (record.getPrice() == null)
                        ? new BigDecimal(0)
                        : record.getPrice().toPlainString(),
                record.getPublisher(), record.getEmail(), record.getPhone(),
                record.getPictureUrl());
    }

    /**
     * getOverride.
     *
     * @return - override
     */
    public static CRUD getOverride() {
        return override;
    }

    /**
     * setOverride.
     *
     * @param overrideSet - overrideSet
     */
    public static void setOverride(final CRUD overrideSet) {
        CRUD.override = overrideSet;
    }
}
